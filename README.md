# "Multi-Armed Bandit Learning in IoT Networks: Learning helps even in non-stationary settings"

This repository contains the LaTeX code of a research poster written by [Lilian Besson](http://perso.crans.org/besson/) and [Remi Bonnefoi](https://remibonnefoi.wordpress.com/), entitled "Multi-Armed Bandit Learning in IoT Networks: Learning helps even in non-stationary settings".

- A 1-page A0-sized PDF poster, [written in LaTeX (LaTeX 2e)](http://www.latex-project.org/), with [the Baposter class/style](http://www.brian-amberg.de/uni/poster).
- This poster will be presented at the [ADDI PhD Student Day](http://addi.asso.insa-rennes.fr/), 3rd of July 2017, at [the IETR lab](http://www.ietr.fr) in Rennes, France.

## *Abstract*
Setting up the future **Internet of Things (IoT)** networks will require to support more and more communicating devices.
We prove that intelligent devices in unlicensed bands can use **Multi-Armed Bandit (MAB)** learning algorithms to improve resource exploitation.
We evaluate the performance of two classical MAB learning algorithms, **UCB1** and **Thomson Sampling**, to handle the decentralized decision-making of Spectrum Access, applied to IoT networks; as well as learning performance with a growing number of intelligent end-devices.

We show that using learning algorithms does help to fit more devices in such networks, even when all end-devices are intelligent and are dynamically changing channel.
In the studied scenario, stochastic MAB learning provides a up to 16% gain in term of successful transmission probabilities, and has near optimal performance even in non-stationary and non-*i.i.d.* settings with a majority of intelligent devices.

## *Key words*:
- Internet of Things,
- Multi-Armed Bandits,
- Reinforcement Learning,
- Cognitive Radio,
- Non-Stationary Bandits.

---

## Links
- The PDF is available here: [Poster JdD  Remi Bonnefoi and Lilian Besson  IoT slotted.en.pdf](https://bitbucket.org/scee_ietr/phd-student-day-ietr-2017-bonnefoi-and-besson/downloads/poster.pdf)
- The LaTeX source code is available here: [Poster JdD  Remi Bonnefoi and Lilian Besson  IoT slotted.en.tex](Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.tex)

![Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.pdf as a PNG image](Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.png)


## Article
This poster is based on an article sent to the [CrownCom 2017](http://crowncom.org/2017/) conference on May 2017.

We presented [this article](https://hal.inria.fr/hal-01575419/document) to the [CrownCom 2017](http://crowncom.org/2017/show/cf-papers) conference in September 2017.
[These slides](http://perso.crans.org/besson/publis/slides/2017_09__Presentation_article_CrownCom_Conference/slides_169.pdf) ([also in 4:3](http://perso.crans.org/besson/publis/slides/2017_09__Presentation_article_CrownCom_Conference/slides.pdf)) were used to present the article at the conference.

- PDF : [BBMKP_CROWNCOM_2017.pdf](https://hal.inria.fr/hal-01575419/document)
- HAL notice : [BBMKP_CROWNCOM_2017](https://hal.inria.fr/hal-01575419/)
- BibTeX : [BBMKP_CROWNCOM_2017.bib](https://hal.inria.fr/hal-01575419/bibtex)

> We got the **Best Paper Award** for our article during the [CrownCom 2017](http://crowncom/2017/) conference!


## Icons license
> Some SVG or EPS icons used for the poster were obtained freely from
the [FlatIcon.com](http://www.flaticon.com/free-icon/) website
([wifi-signal](http://www.flaticon.com/free-icon/wifi-signal_253998#term=antenna&page=1&position=1),
[old-typical-phone](http://www.flaticon.com/free-icon/old-typical-phone_13936#term=phone&page=1&position=42),
[smartphone](http://www.flaticon.com/free-icon/smartphone_149001#term=phone&page=1&position=34)), under the [Creative Commons 3.0 License](http://creativecommons.org/licenses/by/3.0/).

-   Icons ![wifi-signal from FlatIcon.com](fig/wifi-signal.png) made by
    [Madebyoliver](http://www.flaticon.com/authors/madebyoliver "Madebyoliver")
    from [www.flaticon.com](http://www.flaticon.com "Flaticon") is
    licensed by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0")
    ![licensebuttons by](https://licensebuttons.net/l/by/3.0/88x31.png)
-   Icons ![old-typical-phone from FlatIcon.com](fig/old-typical-phone.png) made by
    [Freepik](http://www.freepik.com "Freepik") from
    [www.flaticon.com](http://www.flaticon.com "Flaticon") is licensed
    by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0")
    ![licensebuttons by](https://licensebuttons.net/l/by/3.0/88x31.png)
-   Icons ![smartphone from FlatIcon.com](fig/smartphone.png) made by
    [Freepik](http://www.freepik.com "Freepik") from
    [www.flaticon.com](http://www.flaticon.com "Flaticon") is licensed
    by [CC 3.0 BY](http://creativecommons.org/licenses/by/3.0/ "Creative Commons BY 3.0")
    ![licensebuttons by](https://licensebuttons.net/l/by/3.0/88x31.png)

---

## About
This poster was done for the [PhD Student Day](http://addi.asso.insa-rennes.fr/) 2017, at IETR.

### Copyright
©, 2017, [Rémi Bonnefoi](https://remibonnefoi.wordpress.com/) & [Lilian Besson](http://perso.crans.org/besson/) ([IETR](http://ietr.fr/), [CentraleSupélec](http://www.centralesupelec.fr/), [SCEE Team](http://www.rennes.supelec.fr/ren/rd/scee/)).

<center><a title="Our poster as a PNG image, or a A0 PDF" href="https://bitbucket.org/scee_ietr/phd-student-day-ietr-2017-bonnefoi-and-besson/downloads/poster.pdf"><img width="700px" src="Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.png" alt="Poster_JdD__Remi_Bonnefoi_and_Lilian_Besson__IoT_slotted.en.pdf"></a></center>

> [I (Lilian Besson)](http://perso.crans.org/besson/) have [started my PhD](http://perso.crans.org/besson/phd/) in October 2016, and this is a part of my **on going** research in 2017.

### :scroll: License ?
This project is publicly published, under the terms of the [MIT Licensed](https://lbesson.mit-license.org/) (file [LICENSE](LICENSE)).

[![Published](https://img.shields.io/badge/Published%3F-accepted-green.svg)](https://hal.inria.fr/hal-01575419)
[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://bitbucket.org/scee_ietr/phd-student-day-ietr-2017-bonnefoi-and-besson/commits/)
[![Ask Me Anything !](https://img.shields.io/badge/Ask%20me-anything-1abc9c.svg)](https://Bitbucket.org/lbesson/ama)
[![Analytics](https://ga-beacon.appspot.com/UA-38514290-17/bitbucket.org/scee_ietr/phd-student-day-ietr-2017-bonnefoi-and-besson/commits/README.md?pixel)](https://bitbucket.org/scee_ietr/phd-student-day-ietr-2017-bonnefoi-and-besson/commits/)

[![ForTheBadge uses-badges](http://ForTheBadge.com/images/badges/uses-badges.svg)](http://ForTheBadge.com)
[![ForTheBadge uses-git](http://ForTheBadge.com/images/badges/uses-git.svg)](https://Bitbucket.org/)

[![made-with-latex](https://img.shields.io/badge/Made%20with-LaTeX-1f425f.svg)](https://www.latex-project.org/)
[![ForTheBadge built-with-science](http://ForTheBadge.com/images/badges/built-with-science.svg)](https://Bitbucket.org/lbesson/)
